import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { DetailComponent } from './products/detail/detail.component';
import { ProductsComponent } from './products/products.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';



const routes: Routes = [
  {
    path:'home',
    component:HomeComponent

  },
  {
    path:'products',
    component:ProductsComponent,

  },
  {
    path:'details/:id',
    component:DetailComponent

  },
  {
    path:'about',
    component: AboutComponent
  },
  {
    path:'shoppingcart',
    component: ShoppingcartComponent
  },
  {
    path: '**',
    component: HomeComponent
  }
  
]



@NgModule({

  imports: [
    RouterModule.forRoot( routes )
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
