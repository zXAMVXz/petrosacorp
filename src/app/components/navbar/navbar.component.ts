import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  items:any = [];

  constructor() { }

  ngOnInit(): void {

    this.items = [
      {
        label:'Inicio',
        routerLink:'/home',
        icon:''
      },
      {
        label:'Productos',
        routerLink:'/products',
        icon:''
      },
      {
        label:'¿Quienes somos?',
        routerLink:'/about',
        icon:''
      },
      {
        label:'',
        routerLink:'/shoppingcart',
        icon:'shopping_cart'
      },

    ]



  }

}
