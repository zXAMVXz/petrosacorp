import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit(): void {

  


  }

  showDetailProduct(idProduct: any) {



    console.log(idProduct);
    this._router.navigate([`details/${idProduct}`])

  }

}
